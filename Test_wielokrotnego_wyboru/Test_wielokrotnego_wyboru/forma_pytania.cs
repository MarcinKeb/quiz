﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Test_wielokrotnego_wyboru
{
    public partial class forma_pytania : Form
    {
        string Polaczenie = "datasource=localhost;port=3306;username=root;password=;;charset=utf8";
        int licznik = 1;
        int SUMA = 0;
        int max_punkty = 0;
        int dobrze = 0;
        int zle = 0;
        string OdpA = "";
        string OdpB = "";
        string OdpC = "";
        string OdpD = "";
        public forma_pytania()
        {
            InitializeComponent();
        }

        private void forma_pytania_Load(object sender, EventArgs e)
        {
            Pokaz_pytania();
        }

        void Pokaz_pytania()
        {
            
            
            MySqlConnection Tpytania = new MySqlConnection(Polaczenie);
            MySqlCommand ZapytaniePytania = new MySqlCommand("select * from quiz_wielo.pytania where pytania.id_testu=" + label4.Text + " and pytania.nr_pytania=" + label3.Text + " ;", Tpytania);
            MySqlDataReader CzytaniePytania;

            Tpytania.Open();
            CzytaniePytania = ZapytaniePytania.ExecuteReader();
            try
            {
                while (CzytaniePytania.Read())
                {
                    string Pytanie = CzytaniePytania.GetString("pytanie");
                    string OdpowiedzA = CzytaniePytania.GetString("odpowiedzA");
                    string OdpowiedzB = CzytaniePytania.GetString("odpowiedzB");
                    string OdpowiedzC = CzytaniePytania.GetString("odpowiedzC");
                    string OdpowiedzD = CzytaniePytania.GetString("odpowiedzD");
                    
                    richTextBox1.Text = Pytanie;
                    checkBox1.Text = OdpowiedzA;
                    checkBox2.Text = OdpowiedzB;
                    checkBox3.Text = OdpowiedzC;
                    checkBox4.Text = OdpowiedzD;

                     OdpA = CzytaniePytania.GetString("odpowiedz_prawidlowaA");
                     label5.Text = OdpA;
                     OdpB = CzytaniePytania.GetString("odpowiedz_prawidlowaB");
                     label6.Text = OdpB;
                     OdpC = CzytaniePytania.GetString("odpowiedz_prawidlowaC");
                     label7.Text = OdpC;
                     OdpD = CzytaniePytania.GetString("odpowiedz_prawidlowaD");
                     label8.Text = OdpD;
                     if (OdpA == "1")
                     {
                         max_punkty += 1;
                     }
                     if (OdpB == "1")
                     {
                         max_punkty += 1;
                     }
                     if (OdpC == "1")
                     {
                         max_punkty += 1;
                     }
                     if (OdpD == "1")
                     {
                         max_punkty += 1;
                     }
                    
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            MySqlConnection Ttesty = new MySqlConnection(Polaczenie);
            MySqlCommand ZapytanieTesty = new MySqlCommand("select * from quiz_wielo.testy where id_testu=" + label4.Text + " ;", Ttesty);
            MySqlDataReader CzytanieTesty;

            Ttesty.Open();
            CzytanieTesty = ZapytanieTesty.ExecuteReader();
            try
            {
                while (CzytanieTesty.Read())
                {
                    string Nazwa = CzytanieTesty.GetString("nazwa");
                    label1.Text = Nazwa;
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            licznik += 1;
            
            label3.Text = Convert.ToString(licznik);
            dobrze = 0;
            zle = 0;
            if (checkBox1.Checked)
            {
                if (OdpA == "1")
                {
                    dobrze += 1;
                }
                if (OdpA == "")
                {
                    zle += 8;
                }
            }
            
            if (checkBox2.Checked)
            {
                if (OdpB == "1")
                {
                    dobrze += 1;
                }
                if (OdpB == "")
                {
                    zle += 8;
                }
            }
            if (checkBox3.Checked)
            {                
                if (OdpC == "1")
                {
                    dobrze += 1;
                }
                if (OdpC == "")
                {
                    zle += 8;
                }
            }
            if (checkBox4.Checked)
            {
                if (OdpD == "1")
                {
                    dobrze += 1;
                }
                if (OdpD == "")
                {
                    zle += 8;
                }
            }
            if (max_punkty != dobrze)
            {
                zle += 4;
            }
            if (dobrze > zle)
            {
                SUMA += 1;
            }
            max_punkty = 0;
            textBox1.Text = Convert.ToString(SUMA);
           
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            


            Pokaz_pytania();
            if (licznik == 10)
            {
                button1.Visible = false;
                button2.Visible = true;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dobrze = 0;
            zle = 0;
            if (checkBox1.Checked)
            {
                if (OdpA == "1")
                {
                    dobrze += 1;
                }
                if (OdpA == "")
                {
                    zle += 8;
                }
            }

            if (checkBox2.Checked)
            {
                if (OdpB == "1")
                {
                    dobrze += 1;
                }
                if (OdpB == "")
                {
                    zle += 8;
                }
            }
            if (checkBox3.Checked)
            {
                if (OdpC == "1")
                {
                    dobrze += 1;
                }
                if (OdpC == "")
                {
                    zle += 8;
                }
            }
            if (checkBox4.Checked)
            {
                if (OdpD == "1")
                {
                    dobrze += 1;
                }
                if (OdpD == "")
                {
                    zle += 8;
                }
            }
            if (max_punkty != dobrze)
            {
                zle += 4;
            }
            if (dobrze > zle)
            {
                SUMA += 1;
            }
            max_punkty = 0;

            if (label11.Text == "")
            {
                string ZapytanieDodajOdpowiedzi = "insert into quiz_wielo.udzielone_odpowiedzi (id_ucznia,id_testu,wynik) values('" + this.label10.Text + "','" + this.label4.Text + "','" + SUMA +  "') ;";

                MySqlConnection TOdpowiedzi = new MySqlConnection(Polaczenie);
                MySqlCommand KwerendaDodajOdpowiedzi = new MySqlCommand(ZapytanieDodajOdpowiedzi, TOdpowiedzi);
                MySqlDataReader CzytanieOdpowiedzi;

                try
                {
                    TOdpowiedzi.Open();
                    CzytanieOdpowiedzi = KwerendaDodajOdpowiedzi.ExecuteReader();
                    

                    while (CzytanieOdpowiedzi.Read())
                    {
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (label11.Text != "")
            {
                string ZapytanieZmienWynik = "update quiz_wielo.udzielone_odpowiedzi set id_udzielone_odpowiedzi='" + label12.Text + "',id_ucznia='" + label10.Text + "',id_testu='" + label4.Text + "',wynik='" + SUMA +  "' where id_udzielone_odpowiedzi='" + label12.Text + "' ;";

                MySqlConnection Twynik = new MySqlConnection(Polaczenie);
                MySqlCommand KwerendaZmienWynik = new MySqlCommand(ZapytanieZmienWynik, Twynik);
                MySqlDataReader Czytaniewynik;

                try
                {
                    Twynik.Open();
                    Czytaniewynik = KwerendaZmienWynik.ExecuteReader();
                    

                    while (Czytaniewynik.Read())
                    {
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            
            this.Hide();
            forma_uczen FormaUczen = new forma_uczen();
            
            FormaUczen.label1.Text = label9.Text;
            
            
            FormaUczen.Show();
            
        }

        

    }
}
