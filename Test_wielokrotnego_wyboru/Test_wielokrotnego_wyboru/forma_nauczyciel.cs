﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Test_wielokrotnego_wyboru
{
    public partial class forma_nauczyciel : Form
    {
        string Polaczenie = "datasource=localhost;port=3306;username=root;password=;;charset=utf8";
        public forma_nauczyciel()
        {
            InitializeComponent();
            load_table_pytania();
            Nazwa_testu_combobox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void load_table_pytania()
        {
            string ZapytaniePokazBazePytan = "select pytania.id_pytania, testy.nazwa, pytania.nr_pytania, pytania.pytanie, pytania.odpowiedzA, pytania.odpowiedzB, pytania.odpowiedzC, pytania.odpowiedzD, pytania.odpowiedz_prawidlowaA, pytania.odpowiedz_prawidlowaB, pytania.odpowiedz_prawidlowaC, pytania.odpowiedz_prawidlowaD from quiz_wielo.pytania join quiz_wielo.testy where pytania.id_testu=testy.id_testu;";
            

            MySqlConnection TPytania = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPodazBazePytania = new MySqlCommand(ZapytaniePokazBazePytan, TPytania);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePytania;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void Nazwa_testu_combobox()
        {
            string Zapytanie = "select DISTINCT nazwa from quiz_wielo.testy  ;";
            MySqlConnection Ttesty = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPokaztesty = new MySqlCommand(Zapytanie, Ttesty);
            MySqlDataReader CzytanieTesty;


            try
            {
                Ttesty.Open();
                CzytanieTesty = KwerendaPokaztesty.ExecuteReader();

                while (CzytanieTesty.Read())
                {
                    string Nazwa = CzytanieTesty.GetString("nazwa");
                    comboBox2.Items.Add(Nazwa);
                    comboBox1.Items.Add(Nazwa);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_testu = "";
            string ZapytanieId = "select * from quiz_wielo.testy where nazwa='" + comboBox2.Text + "';";
            MySqlConnection Ttesty = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPokaztesty = new MySqlCommand(ZapytanieId, Ttesty);
            MySqlDataReader CzytanieTesty;

            try
            {
                Ttesty.Open();
                CzytanieTesty = KwerendaPokaztesty.ExecuteReader();

                while (CzytanieTesty.Read())
                {
                    id_testu = CzytanieTesty.GetString("id_testu");                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            string ZapytaniePokazBazePytan = "select pytania.id_pytania, testy.nazwa, pytania.nr_pytania, pytania.pytanie, pytania.odpowiedzA, pytania.odpowiedzB, pytania.odpowiedzC, pytania.odpowiedzD, pytania.odpowiedz_prawidlowaA, pytania.odpowiedz_prawidlowaB, pytania.odpowiedz_prawidlowaC, pytania.odpowiedz_prawidlowaD from quiz_wielo.pytania join quiz_wielo.testy where pytania.id_testu=testy.id_testu and pytania.id_testu='" + id_testu + "';";
                //"select * from quiz_wielo.pytania where id_testu='" +id_testu +"';";
            //"select pytania.id_pytania, testy.nazwa, pytania.nr_pytania, pytania.pytanie, pytania.odpowiedzA, pytania.odpowiedzB, pytania.odpowiedzC, pytania.odpowiedzD, pytania.odpowiedz_prawidlowaA, pytania.odpowiedz_prawidlowaB, pytania.odpowiedz_prawidlowaC, pytania.odpowiedz_prawidlowaD from quiz_wielo.pytania join quiz_wielo.testy where pytania.id_testu=testy.id_testu;";

            MySqlConnection TPytania = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPodazBazePytania = new MySqlCommand(ZapytaniePokazBazePytan, TPytania);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePytania;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            checkBox8.Checked = false;
            checkBox7.Checked = false;
            checkBox6.Checked = false;
            checkBox5.Checked = false;
            int id;
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["id_pytania"].Value.ToString());
            string Zapytanie = "select * from  quiz_wielo.pytania where id_pytania=" + id + "";

            MySqlConnection TPytania = new MySqlConnection(Polaczenie);
            MySqlCommand Kwerenda = new MySqlCommand(Zapytanie, TPytania );
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(Kwerenda);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                label26.Text = dr["id_pytania"].ToString();
                label5.Text = dr["id_testu"].ToString();
                textBox11.Text = dr["nr_pytania"].ToString();
                richTextBox2.Text = dr["pytanie"].ToString();
                textBox9.Text = dr["odpowiedzA"].ToString();
                textBox8.Text = dr["odpowiedzB"].ToString();
                textBox7.Text = dr["odpowiedzC"].ToString();
                textBox6.Text = dr["odpowiedzD"].ToString();
                if(dr["odpowiedz_prawidlowaA"].ToString() == "1")
                {
                    checkBox8.Checked = true;
                }
                if (dr["odpowiedz_prawidlowaB"].ToString() == "1")
                {
                    checkBox7.Checked = true;
                }
                if (dr["odpowiedz_prawidlowaC"].ToString() == "1")
                {
                    checkBox6.Checked = true;
                }
                if (dr["odpowiedz_prawidlowaD"].ToString() == "1")
                {
                    checkBox5.Checked = true;
                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_logowanie Logowanie = new forma_logowanie();
            Logowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string zaznaczenieA = "";
            string zaznaczenieB = "";
            string zaznaczenieC = "";
            string zaznaczenieD = "";
            if (checkBox8.Checked)
            {
                zaznaczenieA = "1";
            }
            if (checkBox7.Checked)
            {
                zaznaczenieB = "1";
            }
            if (checkBox6.Checked)
            {
                zaznaczenieC = "1";
            }
            if (checkBox5.Checked)
            {
                zaznaczenieD = "1";
            }

            string Zapytanie = "update quiz_wielo.pytania set id_pytania='" + label26.Text + "',id_testu='" + this.label5.Text + "',nr_pytania='" + this.textBox11.Text + "',pytanie='" + this.richTextBox2.Text + "',odpowiedzA='" + this.textBox9.Text + "',odpowiedzB='" + textBox8.Text + "',odpowiedzC='" + this.textBox7.Text + "',odpowiedzD='" + textBox6.Text + "',odpowiedz_prawidlowaA='" + zaznaczenieA + "',odpowiedz_prawidlowaB='" + zaznaczenieB + "',odpowiedz_prawidlowaC='" + zaznaczenieC + "',odpowiedz_prawidlowaD='" + zaznaczenieD +  "' where id_pytania='" + this.label26.Text + "' ;";

            MySqlConnection Tpytania = new MySqlConnection(Polaczenie);
            MySqlCommand Kwerenda = new MySqlCommand(Zapytanie, Tpytania);
            MySqlDataReader Czytaniepytania;

            try
            {
                Tpytania.Open();
                Czytaniepytania = Kwerenda.ExecuteReader();
                MessageBox.Show("Zaktualizowano");

                while (Czytaniepytania.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            load_table_pytania();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label3.Text = ""; label4.Text = ""; label23.Text = "";

            string id_testu = "";
            string ZapytanieId = "select * from quiz_wielo.testy where nazwa='" + comboBox1.Text + "';";
            MySqlConnection Ttesty = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPokaztesty = new MySqlCommand(ZapytanieId, Ttesty);
            MySqlDataReader CzytanieTesty;

            try
            {
                Ttesty.Open();
                CzytanieTesty = KwerendaPokaztesty.ExecuteReader();

                while (CzytanieTesty.Read())
                {
                    id_testu = CzytanieTesty.GetString("id_testu");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            int id;
            id = Convert.ToInt32(dataGridView2.Rows[e.RowIndex].Cells["id_udzielone_odpowiedzi"].Value.ToString());

            string imie, nazwisko, klasa;
            string Zapytanie = "select quiz_wielo.udzielone_odpowiedzi.id_udzielone_odpowiedzi, quiz_wielo.testy.nazwa, quiz_wielo.uczniowie.nazwisko, quiz_wielo.uczniowie.imie, quiz_wielo.uczniowie.klasa, quiz_wielo.udzielone_odpowiedzi.wynik from quiz_wielo.udzielone_odpowiedzi join quiz_wielo.testy on quiz_wielo.udzielone_odpowiedzi.id_testu=quiz_wielo.testy.id_testu and quiz_wielo.udzielone_odpowiedzi.id_testu='" + id_testu + "' join quiz_wielo.uczniowie on quiz_wielo.udzielone_odpowiedzi.id_ucznia=quiz_wielo.uczniowie.id_ucznia and quiz_wielo.udzielone_odpowiedzi.id_udzielone_odpowiedzi='"+id+"';";

            MySqlConnection TPytania = new MySqlConnection(Polaczenie);
            MySqlCommand Kwerenda = new MySqlCommand(Zapytanie, TPytania );
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(Kwerenda);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                imie = dr["imie"].ToString();
                nazwisko = dr["nazwisko"].ToString();
                klasa = dr["klasa"].ToString();
                
                label4.Text = comboBox1.Text;
                label23.Text = dr["wynik"].ToString();
                label3.Text = imie + " " + nazwisko + " " + klasa;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_testu = "";
            string ZapytanieId = "select * from quiz_wielo.testy where nazwa='" + comboBox1.Text + "';";
            MySqlConnection Ttesty = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPokaztesty = new MySqlCommand(ZapytanieId, Ttesty);
            MySqlDataReader CzytanieTesty;

            try
            {
                Ttesty.Open();
                CzytanieTesty = KwerendaPokaztesty.ExecuteReader();

                while (CzytanieTesty.Read())
                {
                    id_testu = CzytanieTesty.GetString("id_testu");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            string ZapytaniePokazBazePytan = "select quiz_wielo.udzielone_odpowiedzi.id_udzielone_odpowiedzi, quiz_wielo.testy.nazwa, quiz_wielo.uczniowie.nazwisko, quiz_wielo.uczniowie.imie, quiz_wielo.uczniowie.klasa, quiz_wielo.udzielone_odpowiedzi.wynik from quiz_wielo.udzielone_odpowiedzi join quiz_wielo.testy on quiz_wielo.udzielone_odpowiedzi.id_testu=quiz_wielo.testy.id_testu and quiz_wielo.udzielone_odpowiedzi.id_testu='" + id_testu + "' join quiz_wielo.uczniowie on quiz_wielo.udzielone_odpowiedzi.id_ucznia=quiz_wielo.uczniowie.id_ucznia;";
                // "' join quiz_wielo.uczniowie where udzielone_odpowiedzi.id_ucznia=uczniowie.id_ucznia ;";
            //string ZapytaniePokazBazePytan = "select udzielone_odpowiedzi.id_udzielone_odpowiedzi, testy.nazwa,  udzielone_odpowiedzi.wynik from quiz_wielo.udzielone_odpowiedzi join quiz_wielo.testy where udzielone_odpowiedzi.id_testu=testy.id_testu and udzielone_odpowiedzi.id_testu='" + id_testu + "';";
            //uczniowie.nazwisko, uczniowie.imie
            MySqlConnection TPytania = new MySqlConnection(Polaczenie);
            MySqlCommand KwerendaPodazBazePytania = new MySqlCommand(ZapytaniePokazBazePytan, TPytania);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazePytania;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView2.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

  


       
    }
}
