﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Test_wielokrotnego_wyboru
{
    public partial class forma_uczen : Form
    {
        string Polaczenie = "datasource=localhost;port=3306;username=root;password=;;charset=utf8";
        public forma_uczen()
        {
            InitializeComponent();
            Pokaz_ucznia();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void Pokaz_ucznia()
        {
            string id = "";
                MySqlConnection Tuczniowie = new MySqlConnection(Polaczenie);
                MySqlCommand ZapytanieUczniowie = new MySqlCommand("select * from quiz_wielo.uczniowie where pesel='" + label1.Text +"' ;", Tuczniowie);
                MySqlDataReader CzytanieUczniowie;

                Tuczniowie.Open();
                CzytanieUczniowie = ZapytanieUczniowie.ExecuteReader();
                try
                {
                while (CzytanieUczniowie.Read())
                {
                     id = CzytanieUczniowie.GetString("id_ucznia");
                    string Imie = CzytanieUczniowie.GetString("imie");
                    string Nazwisko = CzytanieUczniowie.GetString("nazwisko");
                    label24.Text = Imie + " " + Nazwisko;
                    label21.Text = id;
                }
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

              
                MySqlConnection TWyniki = new MySqlConnection(Polaczenie);
                MySqlCommand ZapytanieWyniki = new MySqlCommand("select * from quiz_wielo.udzielone_odpowiedzi where id_ucznia='" + id + "' ;", TWyniki);
                MySqlDataReader CzytanieWyniki;

                TWyniki.Open();
                CzytanieWyniki = ZapytanieWyniki.ExecuteReader();
                try
                {
                    while (CzytanieWyniki.Read())
                    {
                        
                        string id_testu = CzytanieWyniki.GetString("id_testu");
                        if (id_testu == "1") 
                        {
                           label22.Text =  CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                          textBox11.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "2")
                        {
                            label25.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox12.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "3")
                        {
                            label26.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox13.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "4")
                        {
                            label27.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox14.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "5")
                        {
                            label28.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox15.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "6")
                        {
                            label29.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox16.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "7")
                        {
                            label30.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox17.Text = CzytanieWyniki.GetString("wynik");
                        }
                        if (id_testu == "8")
                        {
                            label31.Text = CzytanieWyniki.GetString("id_udzielone_odpowiedzi");
                            textBox18.Text = CzytanieWyniki.GetString("wynik");
                        }
                       
                    }

                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

        }

       

        private void forma_uczen_Load(object sender, EventArgs e)
        {
            Pokaz_ucznia();
            if (textBox11.Text != "") //1
            {
                button2.Enabled = true;
            }
            if (textBox12.Text != "") //2
            {
                button3.Enabled = true;
            }
            if (textBox13.Text != "") //3
            {
                button4.Enabled = true;
            }
            if (textBox14.Text != "") //4
            {
                button5.Enabled = true;
            }
            if (textBox15.Text != "") //5
            {
                button6.Enabled = true;
            }
            if (textBox16.Text != "") //6
            {
                button7.Enabled = true;
            }
            if (textBox17.Text != "") //7
            {
                button8.Enabled = true;
            }
           
///////////////////////////////////////////////////
            if (textBox11.Text == "10") //1
            {
                label11.Visible = true;
            }
            if (textBox12.Text == "10") //2
            {
                label12.Visible = true;
            }
            if (textBox13.Text == "10") //3
            {
                label13.Visible = true;
            }
            if (textBox14.Text == "10") //4
            {
                label14.Visible = true;
            }
            if (textBox15.Text == "10") //5
            {
                label15.Visible = true;
            }
            if (textBox16.Text == "10") //6
            {
                label16.Visible = true;
            }
            if (textBox17.Text == "10") //7
            {
                label17.Visible = true;
            }
            if (textBox18.Text == "10") //8
            {
                label18.Visible = true;
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label4.Text = "1";
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox11.Text;
            FormaPytania.label12.Text = label22.Text;
            FormaPytania.Show();
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label4.Text = "2";
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox12.Text;
            FormaPytania.label12.Text = label25.Text;
            FormaPytania.Show();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label4.Text = "3";
            FormaPytania.label11.Text = textBox13.Text;
            FormaPytania.label12.Text = label26.Text;
            FormaPytania.Show();
            


        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "4";
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox14.Text;
            FormaPytania.label12.Text = label27.Text;
            FormaPytania.Show();
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "5";
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox15.Text;
            FormaPytania.label12.Text = label28.Text;
            FormaPytania.Show();
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "6";
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox16.Text;
            FormaPytania.label12.Text = label29.Text;
            FormaPytania.Show();
           
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "7";
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox17.Text;
            FormaPytania.label12.Text = label30.Text;
            FormaPytania.Show();
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "8";
            FormaPytania.label10.Text = label21.Text;
            FormaPytania.label11.Text = textBox18.Text;
            FormaPytania.label12.Text = label31.Text;
            FormaPytania.Show();
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "9";
            FormaPytania.label10.Text = label21.Text;
       
            FormaPytania.Show();
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_pytania FormaPytania = new forma_pytania();
            FormaPytania.label9.Text = label1.Text;
            FormaPytania.label4.Text = "10";
            FormaPytania.label10.Text = label21.Text;
          
            FormaPytania.Show();
            
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Hide();
            forma_logowanie Logowanie = new forma_logowanie();
            Logowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        

    }
}
