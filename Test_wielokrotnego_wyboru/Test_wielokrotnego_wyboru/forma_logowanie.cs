﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Test_wielokrotnego_wyboru
{
    public partial class forma_logowanie : Form
    {
        string Polaczenie = "datasource=localhost;port=3306;username=root;password=;;charset=utf8";
        public forma_logowanie()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
               
                MySqlConnection Tnauczyciele = new MySqlConnection(Polaczenie);
                MySqlConnection Tuczniowie = new MySqlConnection(Polaczenie);

                MySqlCommand ZapytanieNauczyciel = new MySqlCommand("select * from quiz_wielo.nauczyciel where id_nauczyciela='" + textBox1.Text + "'and haslo='" + textBox2.Text + "' ;", Tnauczyciele);
                MySqlCommand ZapytanieUczniowie = new MySqlCommand("select * from quiz_wielo.uczniowie where pesel='" + textBox1.Text + "'and haslo='" + textBox2.Text + "' ;", Tuczniowie);
                MySqlDataReader CzytanieNauczyciel;
                MySqlDataReader CzytanieUczniowie;

                Tnauczyciele.Open();
                Tuczniowie.Open();

                CzytanieNauczyciel = ZapytanieNauczyciel.ExecuteReader();
                CzytanieUczniowie = ZapytanieUczniowie.ExecuteReader();

                int nauczyciel = 0;
                int uczen = 0;


                while (CzytanieNauczyciel.Read())
                {
                    nauczyciel += 1;
                }
                while (CzytanieUczniowie.Read())
                {
                    uczen += 1;
                }

                if ((nauczyciel == 1) || (uczen == 1))
                {
                    if (nauczyciel == 1)
                    {
                        MessageBox.Show("Zalogowano jako Nauczyciel");
                        this.Hide();
                        forma_nauczyciel FormaNauczyciel = new forma_nauczyciel();
                        FormaNauczyciel.label1.Text = textBox1.Text;
                        FormaNauczyciel.ShowDialog();

                    }
                    if (uczen == 1)
                    {
                        MessageBox.Show("Zalogowano jako Uczeń");
                        this.Hide();
                        forma_uczen FormaUczen = new forma_uczen();
                        FormaUczen.label1.Text = textBox1.Text;
                        FormaUczen.ShowDialog();
                    }


                }
                else if ((nauczyciel > 1) || (uczen > 1))
                {
                    MessageBox.Show("Powtarzające się rekordy");
                }
                else
                    MessageBox.Show("Nie prawidłowe login lub hasło");

                Tnauczyciele.Close();
                Tuczniowie.Close();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
    }
}
